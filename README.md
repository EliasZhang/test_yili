# my-vue-app

This object is used generate risk map. Leaflet, OpenLayers and VueLeaflet have been tested.
After comparing, OpenLayers is the best package for generating a map.

## BackEnd Setup 
My backEnd is based on Python. Make sure that Python is installed before setting up BackEnd.
```
cd ../my-vue-app/BackEnd/yili-uris
python3 simple-cors-http-server.py
```

## Setup development environment
```
cd ../my-vue-app
yarn global add @vue/cli
yarn global add @vue/cli-service-global
yarn install
```

### Run FrontEnd
```
cd ../my-vue-app
yarn run serve

```
### Click newMap in the website to get the map

### Function 1
Showing different risk according to filter 
![alt text](./READMEIMAGE/countryrisk.png)

### Function 2
Clicking on country and showing country risk detail
![alt text](./READMEIMAGE/countrydetail.png)

### Function 3
Clicking on facility, and showing the facility and suppliers relationship
![alt text](./READMEIMAGE/facility.png)

### Function 4
Showing the satellite images in certain view level (There are some bugs)
![alt text](./READMEIMAGE/images.png)

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
